const getNum = (time) =>
  new Promise((resovle, reject) => {
    setTimeout(() => {
      if (Math.random() > 0.5) {
        // reject(0)
        // return;
      }
      resovle(1);
    }, time);
  });

const sum = async () => {
  console.log('sum----start');
  const n1 = await getNum(10);
  console.log('n1', n1);
  const n2 = await getNum(10);
  console.log('n2', n2);
  return n1 + n2;
};

// sum().then((data) => {
//   console.log('data',data);
// },(error) => {
//   console.log('error',error);
// });

// async 函数调用方式跟普通函数调用方式一样。返回promise

const sum1 = function* () {
  let n = yield getNum(100);
  console.log('n', n);
  let n1 = yield getNum(200);
  console.log('n1', n1);
  return n + n1;
};

const o = sum1(); // 函数调用方式跟普通函数调用方式一样
// console.log(run); // run 迭代器对象

// run.next().value.then(res => {
//   run.next(res).value.then(res => {
//     console.log(run.next(res));
//   })
// })

function run(o) {
  return new Promise((resovle, reject) => {
    (function next(val) {
      const res = o.next(val);
      console.log(res);
      if (res.done) return resovle(res.value);
      res.value
        .then((value) => {
          next(value);
        })
        .catch((error) => {
          reject(error);
        });
    })();
  });
}

run(o).then((res) => {
  console.log(res);
});

// {value:yield 后面语句返回值,done:true 便利完成 false}
// run.next();
